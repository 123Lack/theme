<?php

namespace Theme\Providers;

use Ceres\Config\CeresConfig;
use Ceres\Contexts\CategoryContext;
use Ceres\Contexts\CategoryItemContext;
use Ceres\Contexts\ChangeMailContext;
use Ceres\Contexts\CheckoutContext;
use Ceres\Contexts\GlobalContext;
use Ceres\Contexts\ItemSearchContext;
use Ceres\Contexts\OrderConfirmationContext;
use Ceres\Contexts\OrderReturnContext;
use Ceres\Contexts\PasswordResetContext;
use Ceres\Contexts\SingleItemContext;
use Ceres\Contexts\TagSearchContext;
use Ceres\Extensions\TwigItemDataField;
use Ceres\Extensions\TwigJsonDataContainer;
use Ceres\Extensions\TwigLayoutContainerInternal;
use Ceres\Extensions\TwigStyleScriptTagFilter;
use Ceres\Hooks\CeresAfterBuildPlugins;
use Ceres\Widgets\WidgetCollection;
use Ceres\Wizard\ShopWizard\Services\DefaultSettingsService;
use Ceres\Wizard\ShopWizard\ShopWizard;
use IO\Extensions\Constants\ShopUrls;
use IO\Extensions\Functions\Partial;
use IO\Helper\RouteConfig;
use IO\Helper\TemplateContainer;
use IO\Services\ItemSearch\Helper\ResultFieldTemplate;
use IO\Services\UrlBuilder\UrlQuery;
use Plenty\Modules\Plugin\Events\AfterBuildPlugins;
use Plenty\Modules\ShopBuilder\Contracts\ContentWidgetRepositoryContract;
use Plenty\Modules\System\Contracts\WebstoreConfigurationRepositoryContract;
use Plenty\Modules\Webshop\Consent\Contracts\ConsentRepositoryContract;
use Plenty\Modules\Wizard\Contracts\WizardContainerContract;
use Plenty\Plugin\ServiceProvider;
use Plenty\Plugin\Templates\Twig;
use Plenty\Plugin\Events\Dispatcher;
use Plenty\Plugin\ConfigRepository;

class ThemeServiceProvider extends ServiceProvider
{
    
    /**
     * Register the service provider.
     */
    public function register()
    {
    
    }
    /**
     * Boot a template for the footer that will be displayed in the template plugin instead of the original footer.
     */
    public function boot(Twig $twig, Dispatcher $eventDispatcher)
    {
        $eventDispatcher->listen('IO.init.templates', function(Partial $partial)
        {
            $partial->set('footer', 'Theme::content.PageDesign.Partials.Footer');
            $partial->set('header', 'Theme::content.PageDesign.Partials.Header.Header');
            return false;
        }, 0);
        
        $eventDispatcher->listen('IO.tpl.confirmation', function(TemplateContainer $container, $templateData)
        {
            $container->setTemplate('Theme::content.Checkout.OrderConfirmation');
            return false;
        }, 0);
        return false;
    }
}